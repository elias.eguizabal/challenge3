package com.trainee.challenge3.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PostDao {
    @Query("SELECT * FROM databasepost")
    fun getAll(): LiveData<List<DataBasePost>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg post: DataBasePost)
}

@Dao
interface CommentDao {
    @Query("SELECT * FROM databasecomment")
    fun getAll(): LiveData<List<DataBaseComment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg post: DataBaseComment)

    @Query("SELECT * FROM databasecomment WHERE postId = :id")
    fun getCommentsPost(id: Int): List<DataBaseComment>
}


@Database(entities = [DataBasePost::class, DataBaseComment::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract val postDao: PostDao
    abstract val commentDao: CommentDao
}

private lateinit var INSTANCE: DataBase

fun getDatabase(context: Context): DataBase {
    synchronized(DataBase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                DataBase::class.java,
                "Posts"
            ).build()
        }
    }
    return INSTANCE
}