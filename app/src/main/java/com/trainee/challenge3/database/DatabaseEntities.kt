package com.trainee.challenge3.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.trainee.challenge3.domain.Comment
import com.trainee.challenge3.domain.Post

@Entity
data class DataBasePost(
    @PrimaryKey val postId: Int,
    val userId: Int,
    val title: String,
    val body: String
)

@Entity
data class DataBaseComment(
    @PrimaryKey val commentId: Int,
    val postId: Int,
    val name: String,
    val email: String,
    val body: String
)

fun List<DataBasePost>.asPostModel(): List<Post> {
    return map {
        Post(userId = it.userId, id = it.postId, title = it.title, body = it.body)
    }
}

fun List<DataBaseComment>.asCommentModel(): List<Comment> {
    return map {
        Comment(
            postId = it.postId,
            id = it.commentId,
            name = it.name,
            email = it.email,
            body = it.body
        )
    }
}

fun List<Post>.asDBPostModel(): Array<DataBasePost> {
    return this.map {
        DataBasePost(postId = it.id, userId = it.userId, title = it.title, body = it.body)
    }.toTypedArray()
}

fun List<Comment>.asDBCommentModel(): Array<DataBaseComment> {
    return this.map {
        DataBaseComment(
            commentId = it.id,
            postId = it.postId,
            name = it.name,
            email = it.email,
            body = it.body
        )
    }.toTypedArray()
}