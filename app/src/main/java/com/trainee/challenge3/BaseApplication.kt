package com.trainee.challenge3

import android.app.Application
import android.os.Build
import androidx.work.*
import com.trainee.challenge3.work.RefreshPostWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class BaseApplication : Application() {

    private val applicationScope = CoroutineScope(Dispatchers.Default)
    override fun onCreate() {
        super.onCreate()
        initWorker()
    }

    private fun initWorker() {
        applicationScope.launch {
            val constraints = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .apply {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setRequiresDeviceIdle(false)
                    }
                }.build()

            val request = PeriodicWorkRequestBuilder<RefreshPostWorker>(
                repeatInterval = 15,
                TimeUnit.MINUTES
            ).setConstraints(constraints).setBackoffCriteria(
                BackoffPolicy.LINEAR,
                OneTimeWorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            ).build()

            WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
                RefreshPostWorker.WORK_NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                request
            )
        }
    }
}