package com.trainee.challenge3.util

enum class ServerRequest {
    REQUESTING,
    SUCCESS,
    FAIL
}