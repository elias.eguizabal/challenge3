package com.trainee.challenge3.domain

data class Post(val userId: Int,
                val id: Int,
                val title: String,
                val body: String)