package com.trainee.challenge3.ui.commentsView

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.trainee.challenge3.R
import com.trainee.challenge3.databinding.CommentsFragmentBinding
import com.trainee.challenge3.util.ServerRequest
import com.trainee.challenge3.viewmodel.commentsVM.CommentsViewModel

class CommentsFragment : Fragment() {

    private lateinit var viewModel: CommentsViewModel
    private var commentsAdapter: CommentsAdapter? = null
    private lateinit var commentsBinding: CommentsFragmentBinding
    private val args: CommentsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activity?.title = getString(R.string.Comment_Title)
        commentsBinding =
            DataBindingUtil.inflate(inflater, R.layout.comments_fragment, container, false)
        commentsBinding.lifecycleOwner = viewLifecycleOwner
        commentsAdapter = CommentsAdapter()
        commentsBinding.rvComments.layoutManager = LinearLayoutManager(requireContext())
        commentsBinding.rvComments.adapter = commentsAdapter
        setViewListeners()
        return commentsBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CommentsViewModel::class.java)
        viewModel.getComment(args.postId)
        setViewModelListeners()
    }

    private fun setViewModelListeners() {
        viewModel.comments.observe(viewLifecycleOwner, {
            if (!it.isNullOrEmpty()) {
                commentsAdapter?.setListData(it)
                commentsAdapter?.notifyDataSetChanged()
            }
        })

        viewModel.serverRequest.observe(viewLifecycleOwner,{
            when (it) {
                ServerRequest.REQUESTING -> {
                    commentsBinding.swrCommentList.isRefreshing = true
                }
                ServerRequest.SUCCESS -> {
                    commentsBinding.swrCommentList.isRefreshing = false
                }
                ServerRequest.FAIL -> {
                    commentsBinding.swrCommentList.isRefreshing = false
                    Toast.makeText(requireContext(),getString(R.string.Network_Fail_Message_Comments), Toast.LENGTH_SHORT).show()
                }
                else->{commentsBinding.swrCommentList.isRefreshing = false}
            }
        })
    }

    private fun setViewListeners() {
        commentsBinding.swrCommentList.setOnRefreshListener {
            viewModel.getComment(args.postId)
        }
    }

}