package com.trainee.challenge3.ui.commentsView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.trainee.challenge3.R
import com.trainee.challenge3.databinding.CommentLayoutBinding
import com.trainee.challenge3.domain.Comment

class CommentsAdapter : RecyclerView.Adapter<CommentViewHolder>() {

    private var dataList = listOf<Comment>()

    fun setListData(data: List<Comment>) {
        dataList = data
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view: CommentLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            CommentViewHolder.LAYOUT,
            parent,
            false
        )
        return CommentViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.comment = dataList[position]
        }
    }

    override fun getItemCount() = dataList.size

}

class CommentViewHolder(val viewDataBinding: CommentLayoutBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.comment_layout
    }
}
