package com.trainee.challenge3.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.trainee.challenge3.R
import com.trainee.challenge3.databinding.PostLayoutBinding
import com.trainee.challenge3.domain.Post

class PostsAdapter(private val callback: PostClick) : RecyclerView.Adapter<PostViewHolder>() {

    private var dataList = listOf<Post>()

    fun setListData(data: List<Post>) {
        dataList = data
    }

    fun getLisData() = dataList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view: PostLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            PostViewHolder.LAYOUT,
            parent,
            false
        )
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.post = dataList[position]
            it.postCallback = callback
        }
    }

    override fun getItemCount() = dataList.size

}

class PostViewHolder(val viewDataBinding: PostLayoutBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.post_layout
    }
}

open class PostClick(val item: (Post) -> Unit) {
    open fun onClick(post: Post) = item(post)
}