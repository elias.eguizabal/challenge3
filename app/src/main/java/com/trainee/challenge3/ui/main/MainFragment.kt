package com.trainee.challenge3.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.trainee.challenge3.R
import com.trainee.challenge3.databinding.MainFragmentBinding
import com.trainee.challenge3.domain.Post
import com.trainee.challenge3.util.ServerRequest
import com.trainee.challenge3.viewmodel.main.MainViewModel

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private var postAdapter: PostsAdapter? = null
    private lateinit var mainBinding: MainFragmentBinding
    private var forcedUpdate = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activity?.title = getString(R.string.Post_Title)
        mainBinding =
            DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        mainBinding.lifecycleOwner = viewLifecycleOwner
        postAdapter = PostsAdapter(PostClick { goComments(it.id) })
        mainBinding.rvPosts.layoutManager = LinearLayoutManager(requireContext())
        mainBinding.rvPosts.adapter = postAdapter
        setViewListeners()

        return mainBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        setViewModelListeners()
    }

    private fun setViewModelListeners() {
        viewModel.posts.observe(viewLifecycleOwner, {
            if (it != postAdapter?.getLisData() || forcedUpdate) {
                updatePosts(it)
            }
        })

        viewModel.serverRequest.observe(viewLifecycleOwner, {
            when (it) {
                ServerRequest.REQUESTING -> {
                    mainBinding.swrPostList.isRefreshing = true
                }
                ServerRequest.SUCCESS -> {
                    mainBinding.swrPostList.isRefreshing = false
                }
                ServerRequest.FAIL -> {
                    mainBinding.swrPostList.isRefreshing = false
                    Toast.makeText(requireContext(),getString(R.string.Network_Fail_Message_Posts),Toast.LENGTH_SHORT).show()
                }
                else->{mainBinding.swrPostList.isRefreshing = false}
            }
        })
    }

    private fun setViewListeners() {
        mainBinding.swrPostList.setOnRefreshListener {
            forcedUpdate = true
            viewModel.getUpdatePost()
        }
    }

    private fun updatePosts(list: List<Post>) {
        postAdapter?.setListData(list)
        postAdapter?.notifyDataSetChanged()
        forcedUpdate = false
        mainBinding.swrPostList.isRefreshing = false
    }

    private fun goComments(postId: Int) {
        findNavController().navigate(
            MainFragmentDirections.actionMainFragmentToCommentsFragment(
                postId
            )
        )
    }

}
