package com.trainee.challenge3.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.trainee.challenge3.database.getDatabase
import com.trainee.challenge3.repository.DBRepository
import retrofit2.HttpException

class RefreshPostWorker(appContext: Context, parameters: WorkerParameters) :
    CoroutineWorker(appContext, parameters) {

    companion object {
        const val WORK_NAME = "Fetch Post Data"
    }

    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val dbRepository = DBRepository(database)
        dbRepository.refreshPost()
        return try {
            dbRepository.refreshPost()
            Result.success()
        } catch (e: HttpException) {
            Result.retry()
        }
    }
}