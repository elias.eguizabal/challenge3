package com.trainee.challenge3.viewmodel.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.trainee.challenge3.database.getDatabase
import com.trainee.challenge3.repository.DBRepository
import com.trainee.challenge3.util.ServerRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val database = getDatabase(application)
    private val dbRepository = DBRepository(database)

    init {
        getUpdatePost()
    }

    val serverRequest = MutableLiveData<ServerRequest>()

    fun getUpdatePost() {
        viewModelScope.launch {
            try {
                serverRequest.value = ServerRequest.REQUESTING
                dbRepository.refreshPost()
                serverRequest.value = ServerRequest.SUCCESS
            } catch (e: Exception) {
                serverRequest.value = ServerRequest.FAIL
            }
        }
    }

    val posts = dbRepository.posts

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
