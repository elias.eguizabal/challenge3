package com.trainee.challenge3.viewmodel.commentsVM

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.trainee.challenge3.database.getDatabase
import com.trainee.challenge3.repository.DBRepository
import com.trainee.challenge3.util.ServerRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class CommentsViewModel(application: Application) : AndroidViewModel(application) {

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val database = getDatabase(application)
    private val dbRepository = DBRepository(database)

    val serverRequest = MutableLiveData<ServerRequest>()

    fun getComment(post: Int) {
        viewModelScope.launch {
            try {
                serverRequest.value = ServerRequest.REQUESTING
                dbRepository.getCommentsPost(post)
                serverRequest.value = ServerRequest.SUCCESS
            } catch (e: Exception) {
                serverRequest.value = ServerRequest.FAIL
            }
        }
    }

    val comments = dbRepository.comments

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}