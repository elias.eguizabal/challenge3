package com.trainee.challenge3.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.trainee.challenge3.database.*
import com.trainee.challenge3.domain.Comment
import com.trainee.challenge3.domain.Post
import com.trainee.challenge3.network.Network
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.await

class DBRepository(private val database: DataBase) {

    val posts: LiveData<List<Post>> = Transformations.map(database.postDao.getAll()) {
        it.asPostModel()
    }

    val comments: MutableLiveData<List<Comment>> by lazy {
        MutableLiveData<List<Comment>>()
    }

    suspend fun refreshPost() {
        withContext(Dispatchers.IO) {
            val postList = Network.fetchData.getPostListAsync().await()
            database.postDao.insertAll(*postList.asDBPostModel())
        }
    }

    suspend fun getCommentsPost(post: Int) {
        Log.d("Comment","Trying")

        withContext(Dispatchers.IO) {
            try {
                comments.postValue(database.commentDao.getCommentsPost(post).asCommentModel())
                Log.d("Comment","FormDataBase")
            } catch (e: Exception) {
                Log.d("Comment",e.message.toString())
                comments.postValue(listOf())
            }

            val commentsTemp = Network.fetchData.getCommentLis(post).await()
            Log.d("Comment","Form WEB")
            database.commentDao.insertAll(*commentsTemp.asDBCommentModel())
            comments.postValue(commentsTemp)
        }
    }

}