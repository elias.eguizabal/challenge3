package com.trainee.challenge3.network

import com.trainee.challenge3.domain.Comment
import com.trainee.challenge3.domain.Post
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface FetchData {
    @GET("posts")
    fun getPostListAsync(): Call<List<Post>>

    @GET("comments?")
    fun getCommentLis(@Query("postId") postId: Int): Call<List<Comment>>
}

object Network {
    // Configure retrofit to parse JSON and use coroutines
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val fetchData: FetchData = retrofit.create(FetchData::class.java)
}